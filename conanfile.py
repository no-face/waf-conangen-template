#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
from os import path

class Recipe(ConanFile):
    name     = "waf-example"
    version  = "0.1.0"
    license  = "XYZ"
    settings = "os", "compiler", "build_type", "arch"

    requires       = (
        #Add your conan requirements, like:
        #'Poco/1.8.0@pocoproject/stable',
    )

    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.7@noface/stable"
    )

    test_requirements = (
        # Define your test requirements, example:
        # "Catch/1.9.4@uilianries/stable",
        # "FakeIt/2.0.4@noface/stable",
    )

    generators = "Waf"
    exports_sources = "src/*", "wscript"

    options = {
        "shared"    : [True, False],
    }
    default_options = (
        "shared=True",
    )

    def build_requirements(self):
        if not self.should_build_tests: #see 'should_build_tests' property below
            return

        for r in self.test_requirements:
            # Add as build requirements because they will not be 'exported'
            # and are needed only during build
            self.build_requires(r)

    def imports(self):
        # Copy waf executable to project folder
        self.copy("waf", dst=".")

        self.copy("*.dll", dst="bin", src="bin") # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        opts = self.waf_options()

        cmd = 'waf configure build -o build {}'.format(opts)
        self.output.info(cmd)
        self.run(cmd, cwd = self.build_folder)

    def package(self):
        self.run("waf install", cwd=self.build_folder)

    def package_info(self):
        self.cpp_info.libs = ['example']

    #################################### Helpers ###########################################

    def waf_options(self):
        opts = []

        if self.settings.build_type == "Debug":
            opts.append("--debug")
        else:
            opts.append("--release")

        if self.options.shared:
            opts.append("--shared")

        install_dir = getattr(self, "package_folder", path.abspath(path.join(".", "package")))

        opts.append("--prefix='%s'" % install_dir)
        opts.append("--build-tests={}".format(self.should_build_tests))

        return " ".join(opts)

    @property
    def should_build_tests(self):
        # Build tests only if running in user folder
        # (not in local cache) and is developing

        return not self.in_local_cache and self.develop
