#include "example.h"

namespace example {

std::string hello(const std::string &name){
    return "Hello " + name + "!";
}

}//namespace
