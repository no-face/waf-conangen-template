#ifndef LIB_EXAMPLE_H
#define LIB_EXAMPLE_H

#include <string>

namespace example {

std::string hello(const std::string & name="world");

}//namespace

#endif // LIB_EXAMPLE_H
