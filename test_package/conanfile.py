#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
import os
from os import path

username = os.getenv("CONAN_USERNAME", "myuser")
channel  = os.getenv("CONAN_CHANNEL", "testing")
version  = "0.1.0"
name     = "waf-example"

class TestRecipe(ConanFile):
    settings = "os", "compiler", "arch"
    #requires = (
    #    "%s/%s@%s/%s" % (name, version, username, channel),
    #)
    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.7@noface/stable"
    )

    generators = "Waf"
    #exports = "./*.cpp", "wscript"
    exports = "*"

    def imports(self):
        self.copy("*.dll"   , src="bin", dst="bin")
        self.copy("*.dylib*", src="lib", dst="bin")

    def build(self):
        cmd = 'waf -v configure build -o "{}"'.format(path.join(self.build_folder, 'build'))

        self.output.info("Running cmd '{}' in '{}'".format(cmd, self.source_folder))

        self.run("ls", cwd=self.source_folder)
        self.run(cmd, cwd=self.source_folder)

    def test(self):
        example_cmd = './build/example'
        self.output.info("running test: '{}' in '{}'".format(example_cmd, self.build_folder))
        self.run(example_cmd, cwd=self.build_folder)
