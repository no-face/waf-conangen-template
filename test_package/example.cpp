#include <iostream>

#include "example/example.h"

int main(){
    std::cout << "*************** start example ********************" << std::endl;

    std::cout << example::hello() << std::endl;

    std::cout << "*************** finish example *******************" << std::endl;

    return 0;
}
